# nx

Documentation for nx (hexacopter with onboard NUC) 


# Drone Assembly Guide


<p align="center">
    <img src="nx_pics/finished_nx.png" width="456" height="341" >
</p>




<p align="center">
    Finished Drone
</p>


<img src="nx_pics/2_parts.jpeg" alt="drawing" width="500" />

Parts
# Part List:

1- Base Plate            x 1  <br>
2- Top Plate             x 1  <br>
3- Arms                  x 6  <br>
4- Arm Sleeves           x 6  <br>
5- Feet                  x 6  <br>
6- Prop Guards           x 12 <br>
7- Snail Motor           x 6  <br> 
8- Propeller             x 6  <br>
9- Motor Guard           x 6  <br>
10- PDB                  x 1  <br>
11- Bullet Connector (F) x 18 <br>
12- Bullet Connector (M) x 18 <br>
13- ESC Module           x 6  <br>
14- Teensy               x 1  <br>
15- Voxl                 x 1  <br>
16- Voxl Base            x 1  <br>
17- Vicon Dots           x 6  <br>
18- Thin Metal Rods      x 2  <br>
19- Thick Metal Rods     x 4  <br>
20- M2x14 Screws         x 12 <br>
21- M2.5x10 Screws       x 22 <br>
22- M2x8 Screws          x 18 <br>
23- M2.5x6+6 Hex Spacer  x 4  <br> 
24- M2.5x6 Screws        x 4  <br>
25- Voxl Base Dampers    x 4  <br>
26- Snail Motor Screws   x 12 <br>
27- Teensy 10-wire Coord x 1  <br>
28- PDB Plastic Screws   x 4  <br>
29- PDB Power coord (F)  x 1  <br>
30- Voxl Power Coord (M) x 1  <br>
31- Power Converter      x 1  <br>
32- Converter Mount      x 1  <br>
33- Intel Nuc            x 1  <br>
34- Velcro Strap         x 1  <br>

Workshop drone parts inventory: https://docs.google.com/spreadsheets/d/1Wlv0AggwJEXu4AvRRExc7lY3p_MQ_X4-Evwyve9mzvk/edit?usp=sharing

# Instructions

IMPORTANT: Motor 1 must be a clockwise spinning motor. The PDB battery terminal must face motor 5. Motor numbers on drones must increase counterclockwise.


1-	Start by mounting the Power Distribution Board (PDB) on the drone base. It needs to be electrically insulated so we cut a piece of insulation board in the shape of the PDB with the corners cut out so screws can go through the insulation board. Then place the PDB on top of the tape and fasten it to the base with plastic screws. Cut the excess screws with pliers. 



<img src="nx_pics/3_pdb.jpg" alt="drawing" width="400" />

(PDB Picture)   Power Distribution Board 


2-  Take the Electronic Speed Controllers (ESC) and the motors. Solder the male bullet connectors to the motor wires and the females to the ESC wires. Use heat-shrink to cover the conductive connectors.

<img src="nx_pics/4_bullet_connector.jpg" alt="drawing" width="400" />

Male bullet connector (left). <br>
Female bullet connector (right).


<img src="nx_pics/5_finished_esc.jpg" alt="drawing" width="400" />

ESC module with female bullet connector and heat shrink.




3-  Using the drill press drill a hole in each robot arm with a #23 drill bit, between the motor mount and the middle screw hole as shown, to properly fit the motor on the arm. Pass the motor wires through the arm hole and fix the motor to the arm with the motor screws (found inside the motor box).


<img src="nx_pics/6_motors_mount.jpg" alt="drawing" width="400" />

Motor arms with holes and mounted motors



4-  Connect the wires in the back of the arm and fasten the arm sleeve with M2x8 screws.
      a. CCW configuration: With snail side up, blue wire goes left, red wire goes center, and gray goes right. 
      b. CW configuration: With snail side up, blue wire goes right, red wire goes center, and gray goes left.



<img src="nx_pics/7_ccw_config.jpg" alt="drawing" width="400" />  

CCW configuration (left) and CW configuration (right)



5-  Make two holes in each propeller guard, as shown, and fasten the guard with the feet using M2x14 screws (2 prop guards per arm).  


<img src="nx_pics/8_prop_guard.jpg" alt="drawing" width="400" />   

Prop guard with two drill holes between center and side holes


6- Attach arms to base with M2.5x10 screws.  


7- Solder ESC power and ground wires to the PDB, the two thicker wires of the ESC. They likely won’t reach the board, so solder some 16 gauge black and white wires to the corresponding ESC wires and then solder to PDB. Solder the white wire to the positive terminal closest to the arm and black to negative.


8- Solder female power cord to PDB. Connect female battery coord to VOXL battery cord (in VOXL box).


9- Check connections with voltmeter by listening for a beep by touching positive ESC terminals to the battery cord positive terminal, and likewise for negative connections.


<img src="nx_pics/9_pdb_connections.jpg" width="400" />   
PDB connected to ESC wires  

10- To connect the 6 motors to the teensy, take the 10-wire coord from the VOXL box, cut-off one of the ports and unplug the first and last three wires as shown. Pass the ESC wires through the holes in the base so that they come out underneath the drone. In order, solder wires 1 through 6 to motors 1 through 6.  

<img src="nx_pics/10_wire_terminal.jpg" alt="drawing" width="400" />     


Wire terminal after removing 0, 7-9


<img src="nx_pics/11_wireterminal_soldered.jpg" alt="drawing" width="400" />    

Wire soldered to ESC

11- Using a voltmeter, and a highbay battery adjust the volate of the output terminals of the converter to be 19V. Fasten converter to converter mount.

12- Label each motor arm 1-6. Motor 1 must spin clockwise and the order increases counter-clockwise.

13- Attach velcro strap and nuc to top plate.

14- Attach the top using M2.5x10 screws, leaving two screw holes available for two vicon dots to screw in. Fasten the converter mount between arms 5 and 6.

15- Make splitter.

16- Attach 4 vicon dots to the prop guards using hot glue, a thick metal rod, and zip ties, as shown (avoid symmetry). Attach two to the top, using thin metal rods that screw into the top plate, at different heights.


17-  Attach propellers to the corresponding motor. 
		a. Red motors and motor guard spin counterclockwise.
		b. Black motors and motor guards spin clockwise.
		c. Propellers have cw or ccw labels on one of the blades and ccw propellers have a white circle on the bottom.

18-  Attach VOXL to VOXL base with M2.5x6+6 hex spacers and M2.5x6 screws. Push the screws through the black Voxl base and screw in the hex spacers on the other side. Then fit the hex space screw on the Voxl screw holes and fasten with M2.5 nuts. Finally, attach the base to the bottom of the drone with dampers (secure lips of dampers to base of drone with a screwdriver). The VOXL camera must face in the direction between propeller arm 1 and 6 and face away from the drone. 

19-  With double sided tape attach teensy underneath second propeller arm.


<img src="nx_pics/12_voxl_installed.png" alt="drawing" width="400" />   


   
VOXL and Teensy mounted on drone    



20- Place the drone label on the top plate facing arm 2.




## Editing Examples

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/mit-acl/fsw/vehicle-builds/nx.git
git branch -M main
git push -uf origin main
```